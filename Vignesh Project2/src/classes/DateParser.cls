public class DateParser
{
	//This is Vignesh-- making a comment for a testing purpose!!!
	//New comment vignesh - test purpose 27 Jan 2017...
//  --------------------------------------------------------
    //  parseDate; null is invalid Date; yyyy-mm-dd and locale-specific e.g. mm/dd/yyyy or dd/mm/yyyy formats supported
    //  --------------------------------------------------------    
    public static Date parseDate_actual(String inDate) {
    system.debug('Date from XL!!!!!!!!'+inDate);
        Date dateRes = null;
        boolean check =false;
        if(inDate.length() > 10)
        {
            check=true;
        }
        //  1 - Try locale specific mm/dd/yyyy or dd/mm/yyyy    
        try {
            String candDate = inDate.substring(0,Math.min(10,inDate.length()));
            system.debug('from try!!!!!!!'+candDate);
            //Addition of a logic which is, as this parser can work only when date is in mm/dd/yyyy format, 
            //if user enters in dd/mm/yyyy format, we can shift the date and month and display properly.
            //Limitation - This logic works only when dd/mm/yyyy format with dd exceeding 12. {always prefer mm/dd/yyyy}
            List<String> values = candDate.split('/');
            String value1 = values[0];
            String value2 = values[1];
            String value3 = values[2];
                
            system.debug('dateEntered!!!!!!!!!'+value1);
            system.debug('monthEntered!!!!!!!!!'+value2);
            system.debug('YearEntered!!!!!!!!!'+value3);               
                       
            if(Integer.valueOf(value1.trim()) <= 12)
            {
            // grab date portion only m[m]/d[d]/yyyy , ignore time        
                dateRes = Date.parse(candDate);            
                system.debug('final date from if!!!!!!!'+dateRes);
            }
            else
            {   
                candDate = value2+'/'+value1+'/'+value3;
                dateRes = Date.parse(candDate);            
                if(check == true)
                {
                    String dateTimeFormat = String.valueOf(dateRes).substring(0,10);
                    List<String> DTvalues = dateTimeFormat.split('/');
                    String value4 = DTvalues[0];
                    String value5 = DTvalues[1];
                    String value6 = DTvalues[2];
                    dateTimeFormat = DTvalues[6]+'/'+DTvalues[4]+'/'+DTvalues[5];
                    dateRes = Date.valueOf(dateTimeFormat);
                }             
                system.debug('final date from else!!!!!!!'+dateRes);  
            }                        
        }
        catch (Exception e) {}
        if (dateRes == null) {
        //  2 - Try yyyy-mm-dd    
            try {
                String candDate = inDate.substring(0,10);  
                system.debug('from catch!!!!!!!'+candDate);
        // grab date portion only, ignore time, if any
                dateRes = Date.valueOf(candDate);
            }
            catch (Exception e) {} 
        }
        return dateRes;
    }
    public static DateTime parseDateTime(String inDateTime) {            
        String myDateTime = inDateTime;
        String dateFromParseMethod = String.valueOf(parseDate(myDateTime));
        String stringDateTime = dateFromParseMethod+' '+myDateTime.substring(11);
        DateTime dt = datetime.valueOf(stringDateTime);
        system.debug('datetime value!!!!!!!!!'+stringDateTime);
        return dt;
        //System.debug(String.valueOfGmt(dt));- valueOfGmt Returns a Datetime that contains the value of the specified String. The String should use the standard date format “yyyy-MM-dd HH:mm:ss” in the GMT time zone.
    }
    //The below method is used only in GenericImportController..
    public static Date parseDate(String inDate) {
    system.debug('Date from XL!!!!!!!!'+inDate);
        Date dateRes = null;
        //  1 - Try locale specific mm/dd/yyyy or dd/mm/yyyy    
        try {
            String candDate = inDate.substring(0,Math.min(10,inDate.length()));
        // grab date portion only m[m]/d[d]/yyyy , ignore time
            dateRes = Date.parse(candDate);
            system.debug('final date!!!!!!!'+dateRes);
        }
        catch (Exception e) {}
        if (dateRes == null) {
        //  2 - Try yyyy-mm-dd    
            try {
                String candDate = inDate.substring(0,10);  
        // grab date portion only, ignore time, if any
                dateRes = Date.valueOf(candDate);
            }
            catch (Exception e) {} 
        }
        return dateRes;
    }
   /* @isTest
    private static void testParseDate() {
        System.assertEquals(Date.newInstance(2020,1,1), parseDate('2020-01-01'));
        System.assertEquals(Date.newInstance(2020,1,1), parseDate('2020-01-01T01:09:00Z'));
        System.assertEquals(Date.newInstance(2020,1,1), parseDate('01/01/2020'));
        System.assertEquals(Date.newInstance(2020,1,1), parseDate('1/1/2020'));
        System.assertEquals(Date.newInstance(2020,1,1), parseDate('01/01/2020 05:08:00.000-0800'));
        System.assertEquals(null,parseDate(null));
        System.assertEquals(null,parseDate(''));
        System.assertEquals(null,parseDate('ab/de/1201'));
        System.assertEquals(null,parseDate('13/01/2020'));
        System.assertEquals(null,parseDate('2020-13-01'));
                
    }*/
    
}